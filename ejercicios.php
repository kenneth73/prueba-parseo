<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Prueba parseo </title>
</head>
<body>
<?php

$htmlMain = file_get_contents('https://www.masterd.es/cursos-de-formacion-mantenimiento-industrial-g11');
$start = stripos($htmlMain, 'id="listado-grupo"');
$end = stripos($htmlMain, '</ul>', $offset = $start);
$length = $end - $start;
$htmlSection = substr($htmlMain, $start, $length);

preg_match_all('@<span>(.+)</span>@', $htmlSection, $matches); 

$objectsCourses = [];
$listTitle = $matches[1];
foreach ($listTitle as $title) {
    $object = new Course();
    $object->set_title($title);
    array_push($objectsCourses, $object);
}

preg_match_all('@href="(.+)"@', $htmlSection, $matches2);
$listLinks = $matches2[1];

//add descriptions 
for ($i=0; $i<count($listLinks); $i++) {
    $objectsCourses[$i]->set_url($listLinks[$i]);
    addDesctiptionCourse($objectsCourses[$i]);
}


echo '<h2>Títulos, enlaces y descripciones de los cursos</h2> ';
echo '<ol>';
foreach($objectsCourses as $course) {
    echo '<li>';
    echo '<strong>-Curso:</strong>'.$course->get_title(). '<br/> <strong>-Link:</strong>'.$course->get_url(). '<br/> <strong>-Descripción:</strong> '.$course->get_desc().'<br/>' ;
    echo '</li><br/>';
}
echo '</ol>';

//matching
$dataCsv = getDataCsv("cursos_masterd.csv");
for ($i=0; $i<count($objectsCourses); $i++) {
    search_title_in_csv($objectsCourses[$i], $dataCsv);
}

write_xml ("maching_courses.xml", $objectsCourses, 1);
write_xml ("no_maching_courses.xml", $objectsCourses, 0);


function getDataCsv($path) {
    $fp = fopen($path, "r");

    $arrayFilas = [];
    while ($data = fgetcsv ($fp, 200, "\n")) {
        array_push($arrayFilas, $data);
    }

    fclose($fp);

    $cleanData = [];
    //limpiamos las comillas de los datos
    foreach ($arrayFilas as $line) {
        foreach ($line as $palabra) {
            $cleanLine = str_replace("\""," ", $palabra);
            array_push($cleanData, $cleanLine);
        }
    }

    return $cleanData;
}

function search_title_in_csv($course, $dataCoursesCsv) {
    for($i = 1; $i<count($dataCoursesCsv)-1; $i++) {
        $cursoCsv = explode(",", $dataCoursesCsv[$i]);
        $tituloCsv = str_replace(' ', '', $cursoCsv[1]); 
        $idCourseCsv = $cursoCsv[0];
        $cursoScrapping= str_replace(' ', '', $course->get_title()); 
        if (strcasecmp($cursoScrapping, $tituloCsv) == 0) {
            $course->set_id($idCourseCsv);
            $course->set_is_present(1);
        }
    }
}

function addDesctiptionCourse($course) {
    $htmlCourse = file_get_contents($course->get_url());
    preg_match_all('@<p>(.+)</p>@', $htmlCourse, $matches);
    $listDescripcion = $matches[1];
    for ($i = 0; $i <= sizeof($listDescripcion)-2; $i++) {
        $course->set_desc(strip_tags($course->get_desc()).' '.$listDescripcion[$i]);        
    }
}

function write_xml($path, $courses, $maching) { //0=no maching, 1=maching
    $objetoXML = new XMLWriter();
    $objetoXML->openURI($path);
    $objetoXML->setIndent(true);
	$objetoXML->setIndentString("\t");
    $objetoXML->startDocument('1.0', 'utf-8');
    $objetoXML->startElement("courses");
    
    foreach($courses as $course) {
        if ($course->get_is_present() ==  $maching) {
            $objetoXML->startElement("course");
            $objetoXML->startElement("title");
            $objetoXML->text(strip_tags($course->get_title()));
            $objetoXML->endElement();

            $objetoXML->startElement("url");
            $objetoXML->text(strip_tags($course->get_url()));
            $objetoXML->endElement();

            $objetoXML->startElement("id");
            $objetoXML->text(strip_tags($course->get_id()));
            $objetoXML->endElement();

            $objetoXML->startElement("description");
            $objetoXML->text(strip_tags($course->get_desc()));
            
            $objetoXML->endElement();
            $objetoXML->fullEndElement();
        }
    }
    $objetoXML->endElement(); // Fin del nodo raíz
	$objetoXML->endDocument();
}

class Course {
    private $id;
    private $title;
    private $url;
    private $desc;
    private $is_present = 0;//0 = no present, 1 = present (in csv)

    function __construct() {  }

    function set_id($id) {
        $this->id = $id;
    }

    function get_id() {
        return $this->id;
    }

    function set_title($title) {
        $this->title = $title;
    }

    function get_title() {
        return $this->title;
    }

    function set_url($url) {
        $this->url = $url;
    }

    function get_url() {
        return $this->url;
    }

    function set_desc($desc) {
        $this->desc = $desc;
    }

    function get_desc() {
        return $this->desc;
    }

    function set_is_present($is_present) {
        $this->is_present = $is_present;
    }
        
    function get_is_present() {
        return $this->is_present;
    }
}

?>

</body>
</html>